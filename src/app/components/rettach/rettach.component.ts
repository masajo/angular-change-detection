import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { RandomData } from 'src/app/providers/data/random-data.provider';

@Component({
  selector: 'app-rettach',
  templateUrl: './rettach.component.html',
  styleUrls: ['./rettach.component.scss'],
  // Input que recibe el componente para saber si
  // tiene que mostrar los cambios del valor en vivo o no (true/false)
  inputs: ['enVivo']
})
export class RettachComponent implements OnInit {

  constructor(
    public randomDataProvider: RandomData,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
  }

  // El componente padre, le pasará un valor como input "enVivo"
  // y al recbirlo se va a ejecutar el método setter
  set enVivo(valor: boolean) {
    if (valor) {
      // Si es "enVivo" reacoplamos los cambios
      // para que se vean reflejados en la vista
      this.changeDetectorRef.reattach();
    } else {
      // Si NO es "enVivo" desacoplamos los cambios
      // para que la vista no los refleje
      this.changeDetectorRef.detach();
    }
  }




}
