import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DataList } from 'src/app/providers/data/data-list.provider';

@Component({
  selector: 'app-detach',
  templateUrl: './detach.component.html',
  styleUrls: ['./detach.component.scss'],
  // Estrategia DEFAULT (La que viene por defecto)
  // changeDetection: ChangeDetectionStrategy.Default
})
export class DetachComponent implements OnInit {

  constructor(
    public dataListProvider: DataList,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {

    // Vamos a conseguir que haya un cambio en
    // La lista de nombres cada 5 segundos
    // a pesar, de que la lista de nombres va a estar
    // constantemente cambiando

    // 1. Realizamos un DETACH, para que los cambios no se muestren
    this.changeDetectorRef.detach();

    // 2. Planteamos un intervalo que cada 5 segundos realice un
    // detectChanges() para que coja el último cambio que haya habido
    // Es decir, los datos van a estar cambiando constantemente, pero
    // la vista solo se actualizará cada 5 segundos con los valores de
    // ese momento concreto
    setInterval(() => {
      // Forzamos manualmente a que los cambios se comprueben y se
      // se reflejen en la vista
      this.changeDetectorRef.detectChanges();
    }, 5000);
  }
}
