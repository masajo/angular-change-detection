import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-on-push',
  templateUrl: './on-push.component.html',
  styleUrls: ['./on-push.component.scss'],
  // changeDetection: ChangeDetectionStrategy.Default
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnPushComponent implements OnInit {

  nTicks = 0;

  constructor(private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    // Cada segundo nTicks se incrementa en uno
    setInterval(() => {
      this.nTicks++;
      console.log('Nº Ticks:', this.nTicks);
      // Aplicar MarkForCheck para que la vista se actualice
      // Es decir, si tenemos una estrategia ONPUSH, tenemos que
      // Ejecutar, a nuestro gusto, el método markForCheck() para que la
      // Vista muestre el nuevo valor
      this.changeDetectorRef.markForCheck();
    }, 1000);
  }

}
