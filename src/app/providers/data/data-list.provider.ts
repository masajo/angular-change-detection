import * as Mock from 'mockjs';

export class DataList {

  // Simulamos una lista de datos que varia en tiempo real
  /**
   * Método que nos va a devolver constantemente una lista con
   * nuevos nombres aleatorios desde mockjs
   */
  get data() {
    const Random = Mock.Random;
    return [
      Random.first(),
      Random.first(),
      Random.first(),
      Random.cfirst(),
    ]
  }
}
