import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'change-detection';
  // Valor para el ejemplo de Reattach
  live = true;
  // Lista de Datos para Ejemplo Async Pipe
  numeros: number[] = [];
  // Behaviour Subject que recibe por defecto un valor inicial
  numero$ = new BehaviorSubject(this.numeros);

  /**
   * Método que añade un nuevo valor aleatorio
   * a la lista de numeros
   */
  anadir() {

    // Añadimos un valor aleatorio a la lista de números
    this.numeros.push(Math.floor(Math.random() * 100) + 1);

    // Emitimos a través de next() la nueva lista
    // con el elemento añadido
    this.numero$.next(this.numeros);

  }

}
